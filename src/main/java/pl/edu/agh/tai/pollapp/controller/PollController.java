package pl.edu.agh.tai.pollapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.tai.pollapp.model.Answer;
import pl.edu.agh.tai.pollapp.model.Poll;
import pl.edu.agh.tai.pollapp.model.User;
import pl.edu.agh.tai.pollapp.repository.AnswerRepository;
import pl.edu.agh.tai.pollapp.repository.PollRepository;
import pl.edu.agh.tai.pollapp.repository.UserRepository;
import java.util.*;

@Controller
@RequestMapping("/poll")
public class PollController {

    @Autowired
    private PollRepository pollRepository;

    @Autowired
    private AnswerRepository answerRepository;

    @Autowired
    private UserRepository userRepository;

    @ResponseBody
    @GetMapping
    private ResponseEntity getNewestPolls(@RequestParam(value = "page_number") Integer pageNumber, @RequestParam(value = "per_page") Integer pollsPerPage){
        if(pageNumber < 1 || pollsPerPage < 1){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Page<Poll> polls = pollRepository.findAll(PageRequest.of(pageNumber-1, pollsPerPage, new Sort(Sort.Direction.DESC, "created")));
        return ResponseEntity.status(HttpStatus.OK).body(polls.getContent());
    }

    @ResponseBody
    @PostMapping
    private ResponseEntity addNewPoll(@AuthenticationPrincipal User user, @RequestBody Map<String, Object> payload){
        try {
            String question = payload.get("question").toString();
            ArrayList<String> answers = (ArrayList<String>) payload.get("answers");
            User author = userRepository.findByPrincipalId(user.getPrincipalId());
            Poll poll = new Poll(question);
            pollRepository.save(poll);

            poll.addAuthorBidirectionally(author);

            answers.forEach(a -> {
                Answer answer = new Answer(a);
                answer.addPollBidirectionally(poll);
                answerRepository.save(answer);
            });
            return ResponseEntity.status(HttpStatus.CREATED).body(poll);
        }
        catch (NoSuchElementException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

    }

    @ResponseBody
    @PostMapping("/{pollId}/vote/{answerId}")
    private ResponseEntity vote(@PathVariable Long pollId, @PathVariable Long answerId, @AuthenticationPrincipal User user){
        try{
            Poll poll = pollRepository.findById(pollId).get();
            User voter = userRepository.findByPrincipalId(user.getPrincipalId());
            Answer answer = answerRepository.findById(answerId).get();
            voter.getAnswers().removeAll(poll.getAnswers());
            answer.addUserBidirectionally(voter);
            answerRepository.save(answer);
            return ResponseEntity.ok(getStats(pollId));
        }
        catch (NullPointerException | NoSuchElementException e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

    }

    private Map<String, Integer> getStats(Long id){
        Poll poll = pollRepository.findById(id).get();
        Map<String, Integer> stats = new HashMap<>();
        poll.getAnswers().forEach(e -> {
            int votesNumber = e.getUsers().size();
            stats.put(e.getAnswer(), votesNumber);
        });
        return stats;
    }

    @DeleteMapping("/{id}")
    private ResponseEntity removeThePoll(@AuthenticationPrincipal User user, @PathVariable Long id){
        try{
            return removePollAndAllDependencies(user, id);
        } catch (NoSuchElementException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
    }

    private ResponseEntity removePollAndAllDependencies(User user, Long id){
        User author = pollRepository.findById(id).get().getAuthor();
        if(user.getPrincipalId().equals(author.getPrincipalId())){
            Poll poll = pollRepository.findById(id).get();
            author.getCreatedPolls().remove(poll);
            poll.getAnswers().forEach(a ->
                a.getUsers().forEach(u ->
                    u.getAnswers().remove(a))
            );
            pollRepository.delete(poll);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        } else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
    }
}
