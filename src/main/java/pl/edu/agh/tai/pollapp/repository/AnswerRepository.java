package pl.edu.agh.tai.pollapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.agh.tai.pollapp.model.Answer;
import pl.edu.agh.tai.pollapp.model.Poll;

@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {
    Answer findByPollAndAnswer(Poll poll, String answer);
}
