package pl.edu.agh.tai.pollapp.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Poll {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String question;

    @JsonManagedReference
    @ManyToOne
    private User author;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Answer> answers = new HashSet<>();

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;


    public Poll() {
    }

    public Poll(String question) {
        this.question = question;
        this.created = new Date();
    }

    public Poll(String question, Set<Answer> answers, User author) {
        this.question = question;
        this.answers = answers;
        this.created = new Date();
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Set<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(Set<Answer> answers) {
        this.answers = answers;
    }

    public Date getCreated() {
        return created;
    }

    public void addAnswer(Answer answer){
        answers.add(answer);
    }

    public void addAnswerBidirectionally(Answer answer){
        answer.setPoll(this);
        answers.add(answer);
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void addAuthorBidirectionally(User author) {
        this.setAuthor(author);
        author.addPoll(this);
    }

}
