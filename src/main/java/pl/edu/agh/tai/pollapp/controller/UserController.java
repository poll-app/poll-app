package pl.edu.agh.tai.pollapp.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.edu.agh.tai.pollapp.model.User;

@Controller
@RequestMapping("/")
public class UserController {

    @ResponseBody
    @GetMapping("/user")
    public User getUser(@AuthenticationPrincipal User user) {
        return user;
    }

}
