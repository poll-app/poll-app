package pl.edu.agh.tai.pollapp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String answer;

    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "answers")
    private Set<User> users = new HashSet<>();

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Poll poll;

    public Answer() {
    }

    public Answer(String answer) {
        this.answer = answer;
    }

    public Answer(String answer, Set<User> users, Poll poll) {
        this.answer = answer;
        this.users = users;
        this.poll = poll;
    }

    public Long getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Poll getPoll() {
        return poll;
    }

    public void setPoll(Poll poll) {
        this.poll = poll;
    }

    public void addPollBidirectionally(Poll poll){
        poll.addAnswer(this);
        this.poll = poll;
    }

    public void addUserBidirectionally(User user){
        users.add(user);
        user.addAnswer(this);
    }

}
